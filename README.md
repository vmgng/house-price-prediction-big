# House Price Prediction Big

Began project on 10/11/22. We used a Kaggle dataset with many features to train a bigger model.

The dataset and Notebook are provided in the repo.

Feel free to tune the hyperparameters (changing loss function, changing optimizer algorithm, changing number of epochs,
changing number of layers and neurons of each layer, etc.)

Post your questions on the Discord server of ACM Data to discuss.
